import React from "react";
import AppRouter from "./router/AppRouter";
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "./style/theme";
import CssBaseline from "@mui/material/CssBaseline";

const App = () => {
    return (
        <>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <AppRouter />
            </ThemeProvider>
        </>
    );
};

export default App;
