import React from "react";
import { ExampleContainerProps, ExampleScreenProps } from "./example.types";

const ExampleContainer = (
    props: ExampleContainerProps,
    Screen: (props: ExampleScreenProps) => JSX.Element
): JSX.Element => {
    return <Screen message="mister" />;
};

export default ExampleContainer;
