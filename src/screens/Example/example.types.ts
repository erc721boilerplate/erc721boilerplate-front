export interface ExampleScreenProps {
    message: string;
}

// no linting here to keep the template clean
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface ExampleContainerProps {}
