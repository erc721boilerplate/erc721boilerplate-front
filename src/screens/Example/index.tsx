import { ExampleContainerProps } from "./example.types";
import ExampleContainer from "./ExampleContainer";
import ExampleScreen from "./ExampleScreen";

// ts-prune-ignore-next
export default (props: ExampleContainerProps) => ExampleContainer(props, ExampleScreen);
