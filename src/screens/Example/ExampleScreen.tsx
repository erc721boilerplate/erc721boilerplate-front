import React from "react";
import { ExampleScreenProps } from "./example.types";
import Box from "@mui/material/Box";
import { useTheme } from "@mui/material";

const ExampleScreen = ({ message }: ExampleScreenProps) => {
    const theme = useTheme();
    return (
        <Box sx={{ border: 1, borderColor: "primary.main" }}>
            <p style={{ color: theme.palette.primary.main }}>Hello {message} </p>
        </Box>
    );
};

export default ExampleScreen;
