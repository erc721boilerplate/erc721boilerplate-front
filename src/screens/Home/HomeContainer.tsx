import React from "react";
import { HomeContainerProps, HomeScreenProps } from "./home.types";

const HomeContainer = (props: HomeContainerProps, Screen: (props: HomeScreenProps) => JSX.Element): JSX.Element => {
    return <Screen />;
};

export default HomeContainer;
