import React from "react";
import { AppBar, Toolbar, Typography, Button, Paper } from "@mui/material";

const HomeScreen = (): JSX.Element => {
    return (
        <>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        News
                    </Typography>
                    <Button color="inherit">Login</Button>
                </Toolbar>
            </AppBar>

            <Paper>
                <Typography variant="h1">Hello</Typography>
            </Paper>
        </>
    );
};

export default HomeScreen;
