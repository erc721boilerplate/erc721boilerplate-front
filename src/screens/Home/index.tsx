import { HomeContainerProps } from "./home.types";
import HomeContainer from "./HomeContainer";
import HomeScreen from "./HomeScreen";

export default (props: HomeContainerProps) => HomeContainer(props, HomeScreen);
