import { createTheme } from "@mui/material/styles";

export const theme = createTheme(
    {
        components: {
            MuiAppBar: {
                styleOverrides: {
                    root: {
                        backgroundImage: "none",
                    },
                },
            },
        },
        palette: {
            mode: "dark",
        },
    },
    ["dark"]
);
